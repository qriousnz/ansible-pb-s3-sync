S3 Sync playbook
=====================
This configures bucket sync between an SFTP server and an S3 bucket.

# Running the playbook
This generally gets run automatically as a part of an automate solution. If you need to update an existing server for the Skinny solution, do as it follows:

```
ansible-playbook -i inventory install-remote.yml -e @path-to-skinny-environment-configuration -e datadog_api_key=<datadog key> -e s3_landing_bucket=nameofthelandingbucket
```
