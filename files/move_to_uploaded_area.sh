#!/bin/bash
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
source "$SCRIPTPATH/s3-sync.config"

if [ $# -lt 1 ] ; then
    echo "Moves files that are not in use to another folder"
    echo ""
    echo "Usage: $0 <path> [file pattern]"
    echo ""
    exit
fi

COPY_PATH=$1
COPY_PATTERN=${2:-*}
DESTINATION_FOLDER="$(basename $COPY_PATH)"

mkdir -p "$UPLOAD_AREA_PATH/$DESTINATION_FOLDER"

cd "$COPY_PATH"
for DIRECTORY in $(find . -type d \( ! -regex '.*/\..*' \) | sed 's/\.\///g') ; do
  mkdir -p "$UPLOAD_AREA_PATH/$DESTINATION_FOLDER/$DIRECTORY"
done
for FILENAME in $(find . -type f \( ! -regex '.*/\..*' \) -iname "$COPY_PATTERN"| sed 's/\.\///g') ; do
  fuser -s "$FILENAME"
  if [ $? -eq 0 ] ; then
     echo "$FILENAME is in use"
  else
     mv "$FILENAME" "$UPLOAD_AREA_PATH/$DESTINATION_FOLDER/$FILENAME"
  fi
done
