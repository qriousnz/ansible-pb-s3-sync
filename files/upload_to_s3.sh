#!/bin/bash
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
source "$SCRIPTPATH/s3-sync.config"

if [ $# -lt 2 ] ; then
    echo "Uploads files to a bucket/prefix and move them to an uploaded area where they will be kept for a configured amount of time"
    echo ""
    echo "Usage: $0 <Source folder> <S3 Bucket>/Prefix"
    echo ""
    exit
fi

SOURCE_FOLDER="$UPLOAD_AREA_PATH/$1"
S3_DESTINATION=$2
DESTINATION_PREFIX=$1
UPLOAD_ERROR=0
RUN_DATE=$(date "+%Y-%m-%d-%H_%M")
cd "$SOURCE_FOLDER"

/usr/local/bin/aws s3 sync --sse AES256 "$SOURCE_FOLDER" "$S3_DESTINATION/$DESTINATION_PREFIX"
if [  $? -eq 0 ] ; then
    if [ "$(ls $SOURCE_FOLDER | wc -l)" != "0" ] ; then
      mkdir -p "$UPLOADED_AREA_PATH/$DESTINATION_PREFIX/$RUN_DATE"
      mv -f "$SOURCE_FOLDER"/* "$UPLOADED_AREA_PATH/$DESTINATION_PREFIX/$RUN_DATE/"
    fi
else
    UPLOAD_ERROR=1
fi

if [ "$UPLOAD_ERROR" -eq "0" ] ; then
  echo "Deleting files older than $UPLOADED_RETENTION"
  find "$UPLOADED_AREA_PATH/$DESTINATION_PREFIX" -mtime +$UPLOADED_RETENTION
else
  echo "There have been errors during the upload process, please check"
fi
